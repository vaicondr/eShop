package archive;

import cz.cvut.eshop.archive.PurchasesArchive;
import cz.cvut.eshop.archive.RandomizedItem;
import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.Order;
import cz.cvut.eshop.shop.ShoppingCart;
import cz.cvut.eshop.shop.StandardItem;
import org.junit.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class PurchasesArchiveTest {

    private PurchasesArchive purchasesArchive;
    private RandomizedItem randomizedItem;

    @Before
    public void init() {
        purchasesArchive = new PurchasesArchive();
        randomizedItem = new RandomizedItem(new Random());
    }

    @Test
    public void itemSoldReturnsZeroForUnknownItem() {
        Assert.assertEquals(purchasesArchive.getHowManyTimesHasBeenItemSold(randomizedItem),0);
    }

    @Test
    public void soldItemStoredCorrectly() {
        Order order = createOrderFromRandomizedItem();
        purchasesArchive.putOrderToPurchasesArchive(order);

        Assert.assertEquals(1,purchasesArchive.getHowManyTimesHasBeenItemSold(randomizedItem));
    }

    @Test
    public void soldItemAddsCorrectlyInArchive() {
        Order order = createOrderFromRandomizedItem();

        purchasesArchive.putOrderToPurchasesArchive(order);
        purchasesArchive.putOrderToPurchasesArchive(order);

        Assert.assertEquals(2,purchasesArchive.getHowManyTimesHasBeenItemSold(randomizedItem));
        purchasesArchive.printItemPurchaseStatistics();
    }

    private Order createOrderFromRandomizedItem() {
        ArrayList<Item> items = new ArrayList<Item>();
        items.add(randomizedItem);
        //TODO Force arrayList in constructor?!
        ShoppingCart shoppingCart = new ShoppingCart(items);

        return new Order(shoppingCart);
    }

}
