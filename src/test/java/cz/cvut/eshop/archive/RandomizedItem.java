package cz.cvut.eshop.archive;

import cz.cvut.eshop.shop.Item;
import cz.cvut.eshop.shop.StandardItem;

import java.util.Random;

public class RandomizedItem extends Item {
    public RandomizedItem(Random random) {
        super(random.nextInt(Integer.MAX_VALUE), "TestItem", random.nextFloat(), "TestCategory");
    }
}
